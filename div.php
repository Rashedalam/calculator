<?php

include_once 'vendor/autoload.php';

use Rashed\ClassFile\Division;
use Rashed\Displayer\Displayer;

$div = new Division();
$result = $div->div2numbers($_POST['number1'], $_POST['number2']);

$displayer = new Displayer();
$displayer->displayH1($result);
