<?php
include_once 'vendor/autoload.php';

use Rashed\ClassFile\Subtraction;
use Rashed\Displayer\Displayer;

$subtract = new Subtraction();
$result = $subtract->sub2numbers($_POST['number1'], $_POST['number2']);

//$result="The Subtraction of those number is : ".sub2numbers($_POST['number1'],$_POST['number2']);

$displayer = new Displayer();
$displayer->displayH1($result);